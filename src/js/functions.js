function showPopup(popupID) {

	if ( !popupID ) { return false; }

	$.magnificPopup.open({
		items: { src: popupID },
	  	type: 'inline'
	});
}