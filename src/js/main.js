// open popup on click `js-popup` class
$('.js-popup').on('click', function(e) {
	e.preventDefault();

	var target = $(this).attr('href');
	showPopup(target);
});

// scroll to blocks
$('.js-scroll-to').on('click', function(e) {
	e.preventDefault();
	var target = $(this).attr('href');

	if ( typeof target !== 'undefined' ) {
		$('html, body').animate({
			scrollTop: $(target).offset().top + 'px'
		}, 1000);
	}
});

// form submiting
$(document).on('submit', 'form', function(e) {
	e.preventDefault();

    var $form = $(this); var errors = false;

    var getQuery = window.location.search.substr(1);
    var formData = $form.serialize() + '&' + getQuery;

    if ( formData ) {
        $.ajax({
        	url: 'order.php',
        	method: 'POST',
        	data: formData,
        	beforeSend: function() {
        		$form.find('[type="submit"]').attr('disabled', 'disabled');
        	},
        	complete: function() {
        		$form.find('[type="submit"]').removeAttr('disabled');
        	},
        	success: function(data) {
	            if ( data == 'sended' ) {
	                $.magnificPopup.close();
	            } else if ( data == 'sended_excursion' ) {
	            	showPopup('#thanks');
	            } else {
	            	console.log(data);
	            }

              $form.find('input[type="text"], input[type="tel"]').val('');

              // var downloadLink = document.createElement('a');
              // downloadLink.download = 'zhk_kazan_raspisanie.pdf';
              // downloadLink.href = '/zhk_kazan_raspisanie.pdf';
              // downloadLink.click();
	        }
	    });
    }
});

$('.excursion__info-left').on('scrollSpy:enter', function() {

   	if ( !$(this).hasClass('minused') ) {
       	var $block = $(this);
       	$block.addClass('minused');

        setTimeout(function() {
       		$block.find('span').text('2');
       	}, 2000);
   	}

});

$('.excursion__info-left').scrollSpy();