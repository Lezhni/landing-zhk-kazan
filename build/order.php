<?php

if ( $_POST ) {

    require 'phpmailer/PHPMailerAutoload.php';

    $mail = new PHPMailer;

    $mail->CharSet = 'UTF-8';
    $mail->setFrom('kznjk1@gmail.com', 'Бесплатная экскурсия по новостройкам Казани');
    $mail->addReplyTo('kznjk1@gmail.com', 'Бесплатная экскурсия по новостройкам Казани');
    $mail->addAddress('kznjk1@gmail.com');
    $mail->isHTML(true);

    $mail->Subject = $_POST['form_type'];

    $mail->Body = "Имя: {$_POST['contact']}";
    if ( isset($_POST['phone']) ) $mail->Body .= "<br>Телефон: {$_POST['phone']}";
    if ( isset($_POST['form_type']) ) $mail->Body .= "<br>Форма: {$_POST['form_type']}";
    if ( isset($_POST['utm_source']) ) $mail->Body .= "<br>Откуда: {$_POST['utm_source']}";
    if ( isset($_POST['utm_medium']) ) $mail->Body .= "<br>Тип площадки: {$_POST['utm_medium']}";
    if ( isset($_POST['utm_term']) ) $mail->Body .= "<br>Ключевое слово: {$_POST['utm_term']}";
    if ( isset($_POST['utm_campaign']) ) $mail->Body .= "<br>Кампания: {$_POST['utm_campaign']}";
    if ( isset($_POST['utm_content']) ) $mail->Body .= "<br>Содержание компании: {$_POST['utm_content']}";

    if( $mail->send() ) {
        if ( $_POST['form_type'] == 'Запись на экскурсию' ) {
            echo 'sended_excursion';
        } else {
            echo 'sended';
        }
    } else {
        echo $mail->ErrorInfo;
    }
}

die();
